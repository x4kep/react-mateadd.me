import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';
toastr.options = toastrOptions;


let baseUrl = connection.API_BASE_URL;

export function BanUser(userId, isBaned) {
  debugger;
  return dispatch => {
    return axios.patch(baseUrl+'api/players/'+userId,
        {
           "isBaned": isBaned
        }
      )
      .then(function(response){
        console.log(response);
        if(isBaned){
            toastr.success("You succesfully ban");
        }else{
            toastr.success("You succesfully unban");
        }
        debugger;
        dispatch(banUser(response));
      })
      .catch(function(error) {
        console.log(error);
      });
    }
}

function banUser(response) {
  return {
    type: types.BANUSER_SUCCESS,
    response: response
  }
}



