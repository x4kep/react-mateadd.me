import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';

let baseUrl = connection.API_BASE_URL;

export function Users(apiKey) {
  return dispatch => {
    return axios.get(baseUrl+'api/Players?filter={"where":{"role":"user"}}')
      .then(function(response){
        console.log(response);
        dispatch(receiveUsers(response));
      })
      .catch(function(error){
        console.log(error);
      });
    }
}

function receiveUsers(response) {
  return {
    type: types.USER_SUCCESS,
    response: response
  }
}




