import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';

toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function filterGameProfileInit(currentGame) {
  let gameId = 1;
  if(currentGame === "csgo"){
    gameId = 2;
  }else if(currentGame === "lol"){
    gameId = 3;
  }
    debugger;
  return dispatch => {
    return axios.get(baseUrl+'api/GameProfiles/?filter={"include":["gameRank","gameMode","gameRole","gameRegion","game","gameProfileComments",{"gameModes":"gameMode"},{"gameRegions":"gameRegion"},{"gameRoles":"gameRole"},{"player":["country","language"]}],"where":{"gameId":'+gameId+',"isActive":true}}')
      .then(function(response){
        debugger;
        console.log("All GameProfiles");
        console.log(response);
        toastr.success("You have succesfully Get All Users")
        // that.setState( {"addGame":true} );
        dispatch(receiveFilterGameProfileInit(response));
      })
      .catch(function(error){
        debugger;
        console.log(error);
        toastr.error("Something went wrong...")
      });
    }
}

function receiveFilterGameProfileInit(response) {
  return {
    type: types.FILTERGAMEPROFILEINIT_SUCCESS,
    response: response
  }
}


export function filterGameProfile(params,currentGame) {
  debugger;
  let gameId = 1;
  if(currentGame === "csgo"){
    gameId = 2;
  }else if(currentGame === "lol"){
    gameId = 3;
  }
  //PARAMS !!!!
  return dispatch => {
    return axios.get(baseUrl+'api/GameProfiles/filterGamerProfiles/?params='+params)
      .then(function(response){
        debugger;
        console.log("Filterd GameProfiles:");
        console.log(response);
        response = response.data.data;
        let responseFormat = "";
        responseFormat += response.map(function(item){
          return item["playerId"]
        });

        toastr.success("Filtered players result")
        if(response.length === 0){
          console.log("Return Empty Array, with message Could not find players with that criterium"); 
          dispatch(receiveFilterGameProfile([]));
          return ;
        }else{
          debugger;
          console.log("Call getGameProfileById, with right object");
          return responseFormat;
        }
      })
      .then(function(responseFormat){
        debugger;
        if(responseFormat===undefined){
          return [];  
        }else{
          return axios.get(baseUrl+'api/GameProfiles/?filter={"include":["gameRank",{"gameModes":"gameMode"},{"gameRegions":"gameRegion"},{"gameRoles":"gameRole"},{"player":["country","language"]}],"where":{"gameId":'+gameId+', "isActive":true, "playerId":{"inq": ['+responseFormat+']}}}')
        }
      })
      .then(function(responseGameProfiles){
        debugger;
        console.log("GameProfilesFiltered");
        console.log(responseGameProfiles);
        dispatch(receiveFilterGameProfile(responseGameProfiles));
      })
      .catch(function(error){
        debugger;
        console.log(error);
        toastr.error("Something went wrong...")
      });
    }
}

function receiveFilterGameProfile(response) {
  return {
    type: types.FILTERGAMEPROFILE_SUCCESS,
    response: response
  }
}



//Get Single Player Profile 
//http://localhost:100/api/GameProfiles/?filter={"include":[{"gameModes":"gameMode"},{"gameRegions":"gameRegion"},{"gameRoles":"gameRole"},{"player":["country","language"]}],"where":{"gameId":1, "isActive":true, "playerId":1}}
