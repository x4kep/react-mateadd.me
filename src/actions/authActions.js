import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';

import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';
toastr.options = toastrOptions;

let baseUrl = connection.API_BASE_URL;

function getUser(userId, userApiKey){
    axios.get(baseUrl+'api/Players/'+userId+'?access_token='+userApiKey)
    .then(function (response) {
      console.log(response);
      // debugger;
      return response;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export function login(userData) {

  sessionStorage.setItem("username", userData.loginName);
  sessionStorage.setItem("password", userData.loginPassword);

  return dispatch => {
    dispatch(requestLogin(userData));
    return axios.post(baseUrl+'api/Players/login',
      {
        username: userData.loginName,
        password: userData.loginPassword
      }
      ).then(function(response){
        debugger;
        console.log(response);
        sessionStorage.setItem("apiToken", response.data.id);
        return response;
      })
      .then(function(response){
        //Get user
        debugger;
        let userId;
        let userApiKey;
        userId = response.data.userId;
        userApiKey = response.data.id;
        sessionStorage.setItem("userId", userId);
        debugger;
        return axios.get(baseUrl+'api/Players/'+userId+'?access_token='+userApiKey);
      })
      .then(function(response){
        debugger;
        let role = response.data.role;
        sessionStorage.setItem("userRole", role); 
        sessionStorage.setItem("isBaned", response.data.isBaned);

        if(response.data.isBaned){
          sessionStorage.removeItem("apiToken");
          sessionStorage.remove("userId", userId);
          toastr.error("You are baned.");
          window.location.reload();
        }else{
          toastr.success("You have succesfully login");
          dispatch(receiveLogin(response));
        }
      })
      .catch(function(error) {
        console.log(error);
        toastr.error('Login have fail.');
        dispatch(loginError(error));
        throw(error);
      });
    }
}

function requestLogin(creds) {
  return {
    type: types.LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  }
}

function receiveLogin(user) {
  return {
    type: types.LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    user: user
  }
}

function loginError(message) {
  return {
    type: types.LOGIN_ERROR,
    isFetching: false,
    isAuthenticated: false,
    message: message
  }
}

export function register(userData) {
  return dispatch => {
    dispatch(requestRegister(userData));
    return axios.post(baseUrl+'api/Players',
      {
        gender: "male",
        username: userData.registerUsername,
        email: userData.registerEmail,
        password: userData.registerPassword,
        role:"user"
      }
      ).then(function (response) {
        toastr.success("You have succesfully register.");
        dispatch(receiveRegister(response));
      })
      .then(function (response){
        console.log("Registered Promisse After That");
      })
      .catch(function(error) {
        console.log(error);
        dispatch(errorRegister(error));
      });
    }
}

function requestRegister(creds) {
  return {
    type: types.REGISTER_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  }
}

function receiveRegister(user) {
  debugger;
  return {
    type: types.REGISTER_SUCCESS,
    isFetching: false,
    user: user
  }
}

function errorRegister(message) {
  debugger;
  return {
    type: types.REGISTER_ERROR,
    isFetching: false,
    message: message.response.data.error.message
  }
}

export function logout(userData) {
  return dispatch => {
    // dispatch(requestRegister(userData));
    return axios.post(baseUrl+'api/Players/logout?access_token=' + sessionStorage.getItem('apiToken')).
        then(function (response) {
          toastr.success("You have succesfully logout");
        debugger;
        sessionStorage.removeItem("apiToken");
        sessionStorage.removeItem("userRole");
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("password");
        sessionStorage.removeItem("userId");
        sessionStorage.removeItem("isBaned");
        
        //Refactor SET GLOBAL DOMAIN NAME
        window.location.reload();

        dispatch(receiveLogout(response));
      })
      .catch(function(error) {
        debugger;
        console.log(error);
        // dispatch(errorRegister(error));
      });
    }
}

function receiveLogout(response) {
  return {
    type: types.LOGOUT_SUCCESS,
    response: response
  }
}
