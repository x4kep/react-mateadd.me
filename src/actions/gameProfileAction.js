import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';
toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

function getGameProfile(userId) {
  debugger;
  return axios.get(baseUrl+'api/GameProfiles/?filter={"include":["gameRank",{"gameModes":"gameMode"},{"gameRegions":"gameRegion"},{"gameRoles":"gameRole"},"game",{"gameProfileComments":"player"},{"player":["country","language"]}],"where":{"playerId":'+userId+'}}');
}



export function GameProfile(userId) {
console.log("Get Game Profile Init: UserId"+userId);
debugger;
return dispatch => {
    axios.all([getGameProfile(userId)])
        .then(axios.spread(function(gameProfile) {
            debugger;
            dispatch(receiveGameProfile(gameProfile));
        }))
        .catch(function(error) {
            console.log(error);
        });   
    }
}
  

function receiveGameProfile(response) {
  debugger;
  return {
    type: types.GAMEPROFILE_SUCCESS,
    response: response
  }
}



