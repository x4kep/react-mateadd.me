import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';
toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function ReviewComment(senderId, receiverId, text){
    debugger;
    //Loged in user id Sender
    console.log("Sender id is: "+senderId);
    //gameProfile receview id
    console.log("Reciver id is: "+receiverId);
    return dispatch => {
        return axios.post(baseUrl+'api/GameProfileComments',
        {
            text: text,
            playerId: senderId,
            gameProfileId: receiverId,
        }
    )
          .then(function(response){
            console.log(response);
            toastr.success("You have succesfully Post Review")
            dispatch(receiveReviewComment(response));
          })
          .catch(function(error){
            console.log(error);
          });
        }
}

function receiveReviewComment(response) {
    debugger;
    return {
      type: types.REVIEWCOMMENT_SUCCESS,
      response: response
    }
  }


