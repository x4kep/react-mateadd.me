import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';
toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

function getBasicProfile(userId) {
  debugger;
  return axios.get(baseUrl+'api/Players/?filter={"include":["country",{"languages":"language"}],"where":{"id":'+userId+'}}');
}

export function BasicProfile(userId) {
console.log("Get Basic Profile Init: UserId"+userId);
return dispatch => {
    axios.all([getBasicProfile(userId)])
        .then(axios.spread(function(gameProfile) {
            debugger;
            dispatch(receiveBasicProfile(gameProfile));
        }))
        .catch(function(error) {
            debugger;
            console.log(error);
        });   
    }
}
  

function receiveBasicProfile(response) {
  debugger;
  return {
    type: types.BASICPROFILE_SUCCESS,
    response: response
  }
}




