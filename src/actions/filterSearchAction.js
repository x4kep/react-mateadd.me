import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

function ageGenerator(startAge, endAge){
  let age = [];
  for(var i=startAge; i <= endAge; i++){
    age.push(i);
  }
  return age;
}

function getGame() {
  return axios.get(baseUrl+'api/games?filter={"include":["gameRoles","gameModes","gameRanks","gameRegions"]}');
}

function getCountry() {
  return axios.get(baseUrl+'api/Countries');
}

function getLanguage() {
  return axios.get(baseUrl+'api/Languages');
}

export function getSearchForm() {
  let formData = [];
  return dispatch => {
    axios.all([getGame(), getCountry(), getLanguage()])
    .then(axios.spread(function(game, country, language) {
        formData.game = game.data;
        formData.country = country.data;
        formData.language = language.data;
        formData.age = ageGenerator(12, 100);
        dispatch(receiveGetGame(formData));
      }))
      .catch(function(error) {
        console.log(error);
      });;
    }
  }

function receiveGetGame(response) {
  return {
    type: types.GET_GAME_SUCCESS,
    response: response
  }
}


