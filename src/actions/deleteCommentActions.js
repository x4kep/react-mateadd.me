import * as types from '../constants/actionTypes';
import * as connection from '../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../options/toastOptions.js';
toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function DeleteComment(commentId) {
  return dispatch => {
    return axios.delete(baseUrl+'api/GameProfileComments/'+commentId+'?access_token='+sessionStorage.apiToken)
      .then(function(response){
        console.log(response);
        if(response.data.count === 1){
          toastr.success("You succesfully deleted comment");
        }
        debugger;
        // dispatch(deleteComment(response));
      })
      .catch(function(error) {
        console.log(error);
      });
    }
}

function deleteComment(response) {
  return {
    type: types.DELETECOMMENT_SUCCESS,
    response: response
  }
}



