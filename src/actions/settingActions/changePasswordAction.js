import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';

toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

//Oldpassword Newpassword and Userid
export function changePassword(params) {
  return dispatch => {
    return axios.post(baseUrl+'api/Players/change-password/?access_token='+sessionStorage.apiToken,
    {
        "oldPassword": params.oldpassword,
        "newPassword": params.newpassword,
        "id": sessionStorage.userId
    })
      .then(function(response){
        console.log(response);
        toastr.success("You have succesfully change Password");
        debugger;
        // dispatch(receiveChangePassword(response));
      })
      .catch(function(error){
        console.log(error);
        debugger;
        toastr.error(error.response.data.error.message);
      });
    }
}

// function receiveChangePassword(response) {
//   return {
//     type: types.CHANGEPASSWORD_SUCCESS,
//     response: response
//   }
// }




