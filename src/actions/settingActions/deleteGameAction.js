import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';

toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function deleteGame(gameProfileId) {
  return dispatch => {
    return axios.delete(baseUrl+'api/GameProfiles/'+gameProfileId)
      .then(function(response){
        console.log(response);
        toastr.success("You have succesfully Deleted GameProfile");
         // that.setState( {"addGame":true} );
        dispatch(receiveDeleteGame(response));
      })
      .catch(function(error){
        console.log(error);
      });
    }
}

function receiveDeleteGame(response) {
  return {
    type: types.DELETEGAME_SUCCESS,
    response: response
  }
}




