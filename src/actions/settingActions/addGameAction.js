import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';

toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function addGame(gameId, playerId) {
    debugger;
  return dispatch => {
    return axios.post(baseUrl+'api/GameProfiles',
      {
        "isActive": false,
        "experience": "",
        "availability": "",
        "gameId": gameId,
        "playerId": playerId,
        "gameRankId": 0,
        "gameRoleId": 0,
        "gameModeId": 0,
        "gameRegionId": 0
      })
      .then(function(response){
        debugger;
        console.log(response);
        toastr.success("You have succesfully Add GameProfile")
        dispatch(receiveAddGame(response));
      })
      .catch(function(error){
        debugger;
        console.log(error);
        toastr.error("Something went wrong...")
      });
    }
}

function receiveAddGame(response) {
  return {
    type: types.ADDGAME_SUCCESS,
    response: response
  }
}




