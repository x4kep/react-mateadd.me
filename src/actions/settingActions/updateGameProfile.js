import * as types from '../../constants/actionTypes';
import * as connection from '../../constants/connection';
import axios from 'axios';
import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';

toastr.options = toastrOptions;

//let baseUrl = 'http://mate-add-me-api.azurewebsites.net';
let baseUrl = connection.API_BASE_URL;

export function updateGameProfile(params) {
    debugger;
  return dispatch => {
    return axios.post(baseUrl+'api/GameProfiles/updateGameProfile/?params=' + JSON.stringify([params]))
      .then(function(response){
        debugger;
        console.log(response);
        toastr.success("You have succesfully Update GameProfile")
        // dispatch(receiveUpdateGameProfile(response));
      })
      .catch(function(error){
        debugger;
        console.log(error);
        toastr.error("Something went wrong... UpdateGameProfile")
      });
    }
}

function receiveUpdateGameProfile(response) {
  return {
    type: types.UPDATEGAMEPROFILE_SUCCESS,
    response: response
  }
}




