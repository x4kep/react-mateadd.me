import React from 'react';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';

const UserTable = ( {users, onSubmitToggleBanUser} ) => {
    debugger;
    if (users.length === 0) {
        return null;
    }
    return (
            <div className="UserTable">
                <table>
                    <thead>
                        <tr>
                            <th>UserName</th>
                            <th>Email</th> 
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(user =>
                            <tr>
                                <td>{ user.username }</td>
                                <td>{ user.email }</td> 
                                <td>{ user.gender }</td>
                                <td>
                                    { user.isBaned 
                                        ? <Button color="success" className="ml-1" onClick={ () => onSubmitToggleBanUser(user.id,false) }>Unban</Button>   
                                        : <Button color="danger" className="ml-1" onClick={  ()  => onSubmitToggleBanUser(user.id,true) }>Ban</Button>
                                    }
                                </td>
                            </tr>    
                        )}
                    </tbody>
                </table>
            </div>
            );
}

UserTable.PropTypes = {
    users: PropTypes.object.isRequired,
};


export default UserTable;
