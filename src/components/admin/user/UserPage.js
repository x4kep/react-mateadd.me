import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import '../../../styles/user-page.css';
import * as userActions from '../../../actions/admin/userActions';
import * as banUserAction from '../../../actions/admin/banUserAction';
import UserTable from './UserTable';


// Since this component is simple and static, there's no parent container for it.
class UserPage  extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: Object.assign({}, this.props.users),
    }
    this.onSubmitToggleBanUser = this.onSubmitToggleBanUser.bind(this);
  };

  onSubmitToggleBanUser(userId,isBaned){
    console.log("onSubmitToggleBanUser"+isBaned+userId);
    this.props.actions.banUserAction.BanUser(userId, isBaned);
  }

  componentWillReceiveProps(nProps, nState){
    debugger;
  }
  

  componentDidMount(){
    console.log("componentDidMount UserPage");
    this.props.actions.userActions.Users(sessionStorage.apiToken);
  }
  render(){
    debugger;
    const { users } = this.props;
    let usersArray = Object.values(users);
    return (
      <div className="container">
        <div className="pageHeader">
          <h1 className="text-center">Manage users</h1>
        </div>
        {/* <h2>Feature: </h2>
        <ol>
          <li>Display all users</li>
          <li>Admin can ban user</li>
          <li>Pagination</li>
          <li>Search by name</li>
        </ol> */}
        <div className="UserPage">
          <UserTable onSubmitToggleBanUser={ this.onSubmitToggleBanUser } users={ usersArray }/>
        </div>  
      </div>
    );
  };
}


function mapStateToProps(state, ownProps) {
  console.log(state);
  debugger;
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions:{
      userActions: bindActionCreators(userActions, dispatch),
      banUserAction: bindActionCreators(banUserAction, dispatch),
    }
  };
}

UserPage.PropTypes = {
  actions: PropTypes.object.isRequired,
  users: PropTypes.array.isRequired,
};

export default  connect(mapStateToProps, mapDispatchToProps)(UserPage);
