import React from 'react';
import '../../../styles/dashboard-page.css';

// Since this component is simple and static, there's no parent container for it.
const SupportPage = () => {
  return (
    <div className="container">
      <div className="pageHeader">
        <h1 className="text-center">Manage support tickets</h1>
      </div>
      <h2>Feature: </h2>
      <ol>
        <li>Display all suport tickets</li>
        <li>Sort support tickets</li>
      </ol>
      <div className="UserPage">
        
      </div>
    </div>
  );
};

export default SupportPage;
