import React from 'react';
import PropTypes from 'prop-types';
import Header from './common/Header';
import Footer from './footer/Footer';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.
class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container-fluid main-container-margin">
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

App.PropTypes = {
  children: PropTypes.element
};

export default App;
