import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, FormText  } from 'reactstrap';

const ChangePasswordForm = ({onClick, isOpen, onChange, onSubmit, errors, changePasswordSubmit}) => {
  return (
    <div className="LoginForm" >
     <Button className="btn btn-success" name="changePassword" id="changePassword" onClick={onClick}>Change password</Button>            
      <Modal isOpen={isOpen}>
        <form onSubmit={onSubmit}>
        <ModalHeader>Change Password</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Old loginPassword</Label>
            <Input  type="password" onChange={onChange} name="oldpassword" id="oldpassword" placeholder=""  />
            <FormText color="danger">
              {errors.oldpassword}
            </FormText>
          </FormGroup>
          <FormGroup>
            <Label>New password</Label>
            <Input  type="password" onChange={onChange} name="newpassword" id="newpassword" placeholder=""  />
            <FormText color="danger">
              {errors.newpassword}
            </FormText>
          </FormGroup>

          <FormGroup>
            <Label>ReNew Password</Label>
            <Input type="password" onChange={onChange} name="renewpassword" id="renewpassword" placeholder=""  />   
            <FormText color="danger">
            {errors.renewpassword}
            </FormText>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onClick}>Cancel</Button>
          <Button color="primary" type="submit" onClick ={ onSubmit }>Change Password</Button>
        </ModalFooter>
        </form>
      </Modal>
    </div>
  );
};

ChangePasswordForm.PropTypes = {
  onClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired
};


export default ChangePasswordForm;
