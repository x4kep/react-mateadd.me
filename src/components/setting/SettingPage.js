import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {browserHistory} from 'react-router';

import * as gameProfileAction from '../../actions/gameProfileAction.js';
import * as basicProfileAction from '../../actions/basicProfileAction.js';
import * as deleteGameAction from '../../actions/settingActions/deleteGameAction.js';
import * as addGameAction from '../../actions/settingActions/addGameAction.js';
import * as updateBasicProfile from '../../actions/settingActions/updateBasicProfileAction.js';
import * as updateGameProfile from '../../actions/settingActions/updateGameProfile.js';
import * as changePassword from '../../actions/settingActions/changePasswordAction.js';

import '../../styles/setting-page.css';
var FontAwesome = require('react-fontawesome');

import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Link } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import classnames from 'classnames';
import ChangePasswordForm from "./ChangePasswordForm";
import AddGameForm from "./AddGameForm";

var Select = require('react-select');

// Since this component is simple and static, there's no parent container for it.
class SettingPage extends React.Component{
  constructor(props) {
    super(props);
   
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      modalChangePassword: false,
      modalAddGame: false,
      addGameSelected: [1],
      setOptionsRank :[

      ],
      selectedOptionGender: "",
      selectedOptionsLanguage : [],
      selectedOptionsCountry : [],
      selectedOptionsRank :[],
      selectedOptionsRegion : [],
      selectedOptionsMode : [],
      selectedOptionsRole : [],
      selectedBirthday: "",
      selectedBiography: "",
      experience:"",
      availability: "",
      steamLink: "",
      isActive: null,
      oldpassword:"",
      newpassword:"",
      renewpassword:"",
      errors:{}
    };

    this.onChangeLanguage = this.onChangeLanguage.bind(this);
    this.onChangeCountry = this.onChangeCountry.bind(this);
    this.onChangeRank = this.onChangeRank.bind(this);
    this.onChangeRegion = this.onChangeRegion.bind(this);
    this.onChangeRole = this.onChangeRole.bind(this);
    this.onChangeMode = this.onChangeMode.bind(this);
    this.onChangeBirthday = this.onChangeBirthday.bind(this);
    this.onChangeGender = this.onChangeGender.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);

    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.toggleModalChangePassword = this.toggleModalChangePassword.bind(this);
    this.toggleModalAddGame = this.toggleModalAddGame.bind(this);

    this.onChange = this.onChange.bind(this);
    this.deleteGame = this.deleteGame.bind(this);
    this.addGame = this.addGame.bind(this);
    
    this.updateBasicProfile = this.updateBasicProfile.bind(this);
    this.updateGameProfile = this.updateGameProfile.bind(this);
    this.onSubmitChangePassword = this.onSubmitChangePassword.bind(this);
    this.updateGameForm = this.updateGameForm.bind(this);
  }

  updateGameForm(event){
    debugger;
  }
  
  changePasswordFormIsValid(){
    let formIsValid = true;
    let errors = {};
    let oldpassword = this.state.oldpassword;
    let newpassword = this.state.newpassword;
    let renewpassword = this.state.renewpassword;
    //Helper function
    function validatePassword(userPassword){
      var reExp =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
      return !reExp.test(userPassword);
    }

    //Check oldpassword
    if(oldpassword.length === 0){
      errors.oldpassword = "Password can't be empty.";
      formIsValid = false;
    }else if(oldpassword.length < 3){
      formIsValid = false;
      errors.userRePassword = "Password length can't be less then 3 letters.";
    }else if(validatePassword(oldpassword)){
      formIsValid = false;
      errors.oldpassword = "Your password is not valid. Only letters, numbers and underscores are allowed, length 6-15.";
    }

    //Check newpassword
    if(newpassword.length === 0){
      errors.newpassword = "Password can't be empty.";
      formIsValid = false;
    }else if(newpassword.length < 3){
      formIsValid = false;
      errors.userRePassword = "Password length can't be less then 3 letters.";
    }else if(validatePassword(newpassword)){
      formIsValid = false;
      errors.newpassword = "Your password is not valid. Only letters, numbers and underscores are allowed, length 6-15.";
    }

    //Check newpassword
    if(renewpassword.length === 0){
      errors.renewpassword = "Password can't be empty.";
      formIsValid = false;
    }else if(renewpassword.length < 3){
      formIsValid = false;
      errors.userRePassword = "Password length can't be less then 3 letters.";
    }else if(validatePassword(renewpassword)){
      formIsValid = false;
      errors.renewpassword = "Your password is not valid. Only letters, numbers and underscores are allowed, length 6-15.";
    }else if(renewpassword !== newpassword){
      formIsValid = false;
      errors.renewpassword = "Password doesn't match.";
    }
    debugger;
    
    this.setState({errors: errors});
    return formIsValid;
  }
  onRadioBtnClick(addGameSelected) {
    this.setState({ addGameSelected });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  deleteGame(gameProfileId){
   
    console.log("Delete game Function onClick");
    this.props.actions.deleteGameAction.deleteGame(gameProfileId, this);
  }

  addGame(event){
    debugger;
    let gameId = this.state.addGameSelected;
    let playerId = sessionStorage.userId;
    this.props.actions.addGameAction.addGame(gameId, playerId);
  }

  loadGame(){
    this.props.actions.gameProfileAction.GameProfile(userId);
  }

  //UPDATE BASIC PROFILE
  updateBasicProfile(){
    
    let params = {};
    if(this.state.selectedOptionsLanguage.length !== 0){
      let langageAsId = [];
      langageAsId.push(this.state.selectedOptionsLanguage.map(function(language){
        return language.id;
      }));
      console.log("langageAsId");
      console.log(langageAsId);
      params.languageId = langageAsId[0]
    }

    if(this.state.selectedOptionsCountry.length !== 0){
      params.countryId = this.state.selectedOptionsCountry.id
    }

    if(this.state.selectedBiography !== ""){
      params.biography = this.state.selectedBiography
    }

    if(this.state.selectedBirthday !== ""){
      params.birthday = this.state.selectedBirthday
    }

    if(this.state.selectedOptionGender !== ""){
      params.gender = this.state.selectedOptionGender
    }
    
    params.id = sessionStorage.userId;
    
    console.log("Basic Profile: Params");
    console.log(params);


    this.props.actions.updateBasicProfile.updateBasicProfile(params);
  }
  
   //UPDATE GAME PROFILE
   updateGameProfile(event){
    let params = {};
    //Refactor (isTime) map func

    //Set isActive
    if(this.state.isActive !== null){
      params.isActive = this.state.isActive;
    }

    //Set SteamLink
    if(this.state.steamLink !== ""){
      params.steamLink = this.state.steamLink
    }

    //Set Mode
    if(this.state.selectedOptionsMode.length !== 0){
      let modeAsId = [];
      modeAsId.push(this.state.selectedOptionsMode.map(function(mode){
        return mode.id;
      }));
      params.modeId = modeAsId[0]
    }

    //Set Role
    if(this.state.selectedOptionsRole.length !== 0){
      let roleAsId = [];
      roleAsId.push(this.state.selectedOptionsRole.map(function(role){
        return role.id;
      }));
      params.roleId = roleAsId[0]
    }

    //Set Region
    if(this.state.selectedOptionsRegion.length !== 0){
      let regionAsId = [];
      regionAsId.push(this.state.selectedOptionsRegion.map(function(region){
        return region.id;
      }));
      params.regionId = regionAsId[0]
    }

    //Set Rank
    if(this.state.selectedOptionsRank.length !== 0){
      params.gameRankId = this.state.selectedOptionsRank.id
    }

    //Set availability
    if(this.state.availability.length !== 0){
      params.availability = this.state.availability
    }

    //Set experiance
    if(this.state.experience.length !== 0){
      params.experience = this.state.experience
    }

    //Set steam
    params.playerId = sessionStorage.userId;
    params.gameProfileid = event.target.id;

    

    console.log("GameProfileUpdate PARAMS");
    console.log(params);


    this.props.actions.updateGameProfile.updateGameProfile(params);
  }

  onChange(e){
    
    this.setState({ [e.target.name]: e.target.value });
  }

  toggleModalChangePassword() {
    console.log(this);
    this.setState({
      modalChangePassword: !this.state.modalChangePassword,
      errors: {}
    });
  }

  toggleModalAddGame() {
    console.log(this);
    this.setState({
      modalAddGame: !this.state.modalAddGame
    });
  }


  onSubmitChangePassword(event){
    event.preventDefault();
    console.log('onSubmitChangePassword');
    debugger;
    let params = {};
    if(!this.changePasswordFormIsValid()){
      return;
    }


    params.oldpassword = this.state.oldpassword;
    params.newpassword = this.state.newpassword;
    this.props.actions.changePassword.changePassword(params);

  }

  onChangeLanguage(value) {
    this.setState({ selectedOptionsLanguage: value });
  }
  onChangeCountry(value) {
    this.setState({ selectedOptionsCountry: value });
  }
  onChangeRank(value) {
    this.setState({ selectedOptionsRank: value });
  }
  onChangeRegion(value) {
    this.setState({ selectedOptionsRegion: value });
  }
  onChangeRole(value) {
    this.setState({ selectedOptionsRole: value });
  }
  onChangeMode(value) {
    this.setState({ selectedOptionsMode: value });
  }
  onChangeBirthday(event){
    
    this.setState({ selectedBirthday: event.target.value });
  }
  onChangeGender(event){
    this.setState({ selectedOptionGender: event.target.value });
  }

  onChangeIsActive(event){
    this.setState({ isActive: event.target.checked });
  }

  componentWillReceiveProps(state){
    if(state.deleteGame || state.addGame){
      window.location.reload()
    }
  }



  componentWillUpdate(nProps, nState){
   
  }

  componentWillMount(a,b,c){  
    console.log("componentWillMount");
    this.setState({"activeTab":"0"});
  }


  componentDidMount(a, b, c){
    /*REFACTOR PROBABY USER INFO SHOULD GET FROM STATE*/
    let userId = sessionStorage.userId;
    console.log("componentDidMount ProfilePage");
    console.log("Get userGameProfile" + userId);
    this.props.actions.gameProfileAction.GameProfile(userId);
    this.props.actions.basicProfileAction.BasicProfile(userId);
  }
  
  

  render(){

    const { gameProfile, basicProfile, games } = this.props;
    let gameProfileArray = Object.values(gameProfile);
    let basicGameProfileArray= Object.values(basicProfile)
    
    if(games.length === 0 || basicGameProfileArray.length === 0){
      return (<div>Loading...</div>);
    }

   
    console.log("All LOADED");
    console.log("BasicProfile");
    console.log(basicGameProfileArray);
    console.log("Game Profile");
    console.log(gameProfileArray);
    console.log("Set Form Games");
    console.log(games);
    //Fill up FormData
    let languageArray = games[0].language;
    let countryArray = games[0].country;
    let gameArray = Object.values(games[0].game);
    
    let formatLanguage = [];
    formatLanguage.push(basicGameProfileArray[0].languages.map(function(language){
      return language.language;
    }));
    
    function formatMultiSelect(arrayToFormat, nameProp){
      let formated = [];
      formated.push(arrayToFormat.map(function(item){
        console.log(item);
        return item[nameProp];
      }));
      return formated[0];
    }

    //Function that will transform input Language / Mode / Rank / Role 

    console.log("FORMATEDLANGUAGE");
    console.log(formatLanguage);

    return (
      <div className="container">
        <div className="pageHeader">
          <h1 className="text-center">Profile page</h1>
        </div>
        <div className="ProfilePage">
        {/* TODO:MAKE GameSelectorBar Component */}
        <div className="GameSelectorBar">
          <div className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-mateaddme", { 'GameSelectorBar-gameIcon-selected': this.state.activeTab === '0' })} onClick={() => { this.toggle('0'); }}>
            
          </div>
          {gameProfileArray.map(gameProfile =>
              <div key={gameProfile.game.id} className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-"+gameProfile.game.shortName, { 'GameSelectorBar-gameIcon-selected': this.state.activeTab === gameProfile.game.id })} onClick={() => { this.toggle(gameProfile.game.id); }}>

              </div>
          )}
          <div className="GameSelectorBar-AddGameIcon float-right">
            <AddGameForm
              ownedGames = { gameProfileArray }
              toggleModal ={ this.toggleModalAddGame }
              isOpen={ this.state.modalAddGame }
              onChange={ this.onChange }
              onClickRadio = { this.onRadioBtnClick }
              radioActive = { this.state.addGameSelected }
              addGameSubmit = { this.addGame }
            />
          </div>
        </div>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="0">
            <Row>
              <Col>
                <div className="row">
                   <div className="col-md-6">
                    <h4>Basic information</h4>
                    <Form>
                    <FormGroup tag="fieldset">
                      <Label for="gender">Gender</Label>
                      <FormGroup check>
                        <Label check>
                          <Input type="radio" value="male" name="gender"  checked={ this.state.selectedOptionGender === 'male' || basicGameProfileArray[0].gender === "male" }   onChange = { this.onChangeGender } />{' '}
                          Male
                        </Label>
                      </FormGroup>
                      <FormGroup check>
                        <Label check>
                          <Input type="radio" value="female" name="gender" checked={ this.state.selectedOptionGender === 'female' || basicGameProfileArray[0].gender === "female" } onChange = { this.onChangeGender } />{' '}
                          Female
                        </Label>
                      </FormGroup>
                    </FormGroup>
                    <hr/>
                      <FormGroup>
                        <Label for="biography">Biography</Label>
                        <Input type="textarea" name="selectedBiography" id="biography" placeholder="Tell us something about your self." rows="4" 
                          value={ basicGameProfileArray[0].biography}
                          value = 
                                { this.state.selectedBiography === ""
                                    ? basicGameProfileArray[0].biography
                                    : this.state.selectedBiography
                                } 
                          onChange = { this.onChange }
                        />
                        <FormText color="muted">
                          Your biography will be shown at the top of your user profile.
                        </FormText>
                      </FormGroup>
                      <hr/>
                      <FormGroup>
                        <Label for="birthday">Birthday (required)</Label>
                        <Input 
                            min="1920-01-01"
                            max="2100-01-01"
                            type="date" 
                            name="birthday" 
                            id="birthday" 
                            value={ basicGameProfileArray[0].birthday !== null && (basicGameProfileArray[0].birthday).substring(0, 10) } 
                            value = 
                                { this.state.selectedBirthday === ""
                                    ? basicGameProfileArray[0].birthday !== null && (basicGameProfileArray[0].birthday).substring(0, 10)
                                    : this.state.selectedBirthday
                                } 
                            onChange={ this.onChangeBirthday }
                        />
                      </FormGroup>
                      <hr/>
                      <FormGroup>
                        <Label for="country">Country (required)</Label>
                        <Select
                          className="countrySelect"
                          name="selectedOptionsCountry"
                          value = 
                                { this.state.selectedOptionsCountry.length === 0
                                    ? basicGameProfileArray[0].country
                                    : this.state.selectedOptionsCountry
                                }                          
                          options={ countryArray }
                          onChange={ this.onChangeCountry }
                      />
                      </FormGroup>
                      <hr/>
                      <FormGroup>
                        <Label for="language">Language (required)</Label>
                        <Select
                          className="languageSelect"
                          name="selectedOptionsLanguage"
                          value = 
                                { this.state.selectedOptionsLanguage.length === 0
                                    ? formatLanguage[0]
                                    : this.state.selectedOptionsLanguage
                                } 
                          options={ languageArray }
                          onChange={this.onChangeLanguage}
                          multi={true}
                        />
                      </FormGroup>
                      <hr/>
                      <Button className="btn btn-success float-right"
                       onClick={ this.updateBasicProfile }>Save
                        <FontAwesome
                            className='super-crazy-colors ml-2'
                            name='save'
                            size='1x'
                            style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                        />
                      </Button>
                    </Form>
                  </div>
                  <div className="col-md-6">
                    <h4>Basic Actions</h4>
                    <Label for="changePassword">Password</Label><br/>

                    <ChangePasswordForm 
                        onClick={this.toggleModalChangePassword}
                        isOpen={this.state.modalChangePassword}
                        onChange={this.onChange}
                        onSubmit={this.onSubmitChangePassword}
                        errors={this.state.errors}
                      />
                  </div>
                </div>
              </Col>
            </Row>
          </TabPane>
         
         {gameProfileArray.map(gameProfile =>
          <TabPane tabId={gameProfile.game.id}>
            <Row>
              <Col>
                <h4>{gameProfile.game.name}</h4>
                  <div className="row">
                    <div className="col-md-6">
                       <Form onChange= {this.updateGameForm}>
                        <FormGroup>
                          <Label for="gameProfileStatus">
                            Active / Disabled
                            <Input 
                              type="checkbox" 
                              name="isActive" 
                              id="isActive"
                              className="gameProfileStatus"
                              checked = 
                              { this.state.isActive === null
                                  ? gameProfile.isActive 
                                  : this.state.isActive  
                              }
                              onChange = { this.onChangeIsActive }
                             />{' '}  
                          </Label>
                          <FormText color="muted">
                            Checking this other people can find you.
                          </FormText>
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="name">Steam Community Link (required)</Label>
                          <FormText color="muted">
                            Go to https://steamcommunity.com/my to get your link.
                          </FormText>
                          <Input type="text" name="steamLink" id="steamLink" placeholder="steam:id" onChange = { this.onChange }/>
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="solo">Rank (required)</Label>
                            <Select
                              className="helloSelect"
                              name="basicSelect1"
                              value = 
                                { this.state.selectedOptionsRank.length === 0
                                    ? gameProfile.gameRank
                                    : this.state.selectedOptionsRank
                                } 
                              options={ gameArray[gameProfile.game.id-1].gameRanks }
                              onChange={ this.onChangeRank }
                          />
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="modes">Mode (required)</Label>
                          <Select
                            className="helloSelect"
                            name="basicSelect"
                            value = 
                                { this.state.selectedOptionsMode.length === 0
                                    ? formatMultiSelect(gameProfile.gameModes, "gameMode")
                                    : this.state.selectedOptionsMode
                                } 
                            options={gameArray[gameProfile.game.id-1].gameModes}
                            onChange={this.onChangeMode}
                            multi= {true} 
                          />
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="role">Role (required)</Label>
                          <Select
                            className="helloSelect"
                            name="basicSelect"
                            value = 
                                { this.state.selectedOptionsRole.length === 0
                                    ? formatMultiSelect(gameProfile.gameRoles, "gameRole")
                                    : this.state.selectedOptionsRole
                                } 
                            options={gameArray[gameProfile.game.id-1].gameRoles}
                            onChange={this.onChangeRole}
                            multi= {true}
                          />
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="region">Region (required)</Label>
                          <Select
                            className="helloSelect"
                            name="basicSelect"
                            value = 
                                {this.state.selectedOptionsRegion.length === 0
                                    ? formatMultiSelect(gameProfile.gameRegions, "gameRegion")
                                    : this.state.selectedOptionsRegion
                                } 
                            options={gameArray[gameProfile.game.id-1].gameRegions}
                            onChange={this.onChangeRegion}
                            multi= {true}
                          />
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="experience">Experience (required)</Label>
                          <Input 
                            type="textarea" 
                            name="experience" 
                            id="experience" 
                            placeholder="Tell us something about your self." 
                            rows="4" 
                            value = { gameProfile.experience }
                            value = 
                                {this.state.experience.length === 0
                                    ? gameProfile.experience
                                    : this.state.experience
                                }   
                            onChange={ this.onChange }/>  
                          <FormText color="muted">
                            Your experience will be displayed on your user profile for this game.
                          </FormText>
                        </FormGroup>
                        <hr/>
                        <FormGroup>
                          <Label for="availability">Availability (required)</Label>
                          <Input 
                            type="textarea" 
                            name="availability" 
                            id="availability"
                            placeholder="Tell us when you can play." 
                            rows="4" 
                            value = 
                                {this.state.availability.length === 0
                                    ? gameProfile.availability
                                    : this.state.availability
                                }
                            onChange={ this.onChange } />
                          <FormText color="muted">
                            Your availability will be displayed on your user profile for this game.
                          </FormText>
                        </FormGroup>
                        <hr/>
                        <Button className="btn btn-success float-right" name="profileId" id={gameProfile.id} onClick={ this.updateGameProfile }>Save
                          <FontAwesome
                              className='super-crazy-colors ml-2'
                              name='save'
                              size='1x'
                              style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                          />
                        </Button>
                      </Form>
                    </div>
                    <div className="col-md-6">
                      <h4>Basic Actions</h4>
                      <Label for="deleteGame">Delete Game</Label><br/>
                        <Button className="btn btn-danger" name="deleteGame" id="deleteGame" onClick={ () => this.deleteGame(gameProfile.id) }>Delete Game 
                          <FontAwesome
                            className='super-crazy-colors ml-2'
                            name='trash '
                            size='1x'
                            style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                          />
                        </Button>
                      {/*
                        TODO POPUP ASK IF HE IS SURE THAT HE WANT TO DELETE GAME
                      */}
                    </div>
                  </div>
              </Col>
            </Row>
          </TabPane>
          )}
        </TabContent>
        </div>
      </div>
    );
  }
};



function mapStateToProps(state, ownProps) {
  return {
    logedInUserId: state.authReducer.userId,
    gameProfile:   state.gameProfile,
    basicProfile:  state.basicProfile,
    games:         state.filterSearchAction,
    deleteGame:    state.deleteGame,
    addGame:       state.addGame,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions:{
      gameProfileAction: bindActionCreators(gameProfileAction, dispatch),
      basicProfileAction: bindActionCreators(basicProfileAction, dispatch),
      deleteGameAction: bindActionCreators(deleteGameAction, dispatch),
      addGameAction: bindActionCreators(addGameAction, dispatch),
      updateBasicProfile: bindActionCreators(updateBasicProfile, dispatch),
      updateGameProfile: bindActionCreators(updateGameProfile, dispatch),
      changePassword: bindActionCreators(changePassword, dispatch)
    }
  };
}


// SettingPage.PropTypes = {
//   gameProfile: PropTypes.object.isRequired,
//   basicProfile: PropTypes.object.isRequired,
//   games: PropTypes.object.isRequired,
// };

export default  connect(mapStateToProps, mapDispatchToProps)(SettingPage);