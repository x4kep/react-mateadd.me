import React from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup, FormText, Select   } from 'reactstrap';
var FontAwesome = require('react-fontawesome');

const AddGameForm = ({addGameSubmit, toggleModal, isOpen, onChange, onSubmit, errors, onClickRadio, radioActive, ownedGames}) => {
  
  //Disable button that he alredy have.  
  let listOfOwnedGames = [];
  listOfOwnedGames.push(ownedGames.map(function(item){
    return item.game.shortName;
  }));

  return (
    <div className="LoginForm" >
        <Button className="btn btn-success" name="changePassword" id="changePassword" onClick={ toggleModal }>Add Game
            <FontAwesome
                className='super-crazy-colors ml-2'
                name='plus'
                size='1x'
                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
            />
         </Button>            
        <Modal isOpen={isOpen}>
        <Form onSubmit={onSubmit}>
        <ModalHeader>Select Game</ModalHeader>
        <ModalBody>
            <ButtonGroup>
                <Button color="primary" onClick={() => onClickRadio(1)} active={radioActive === 1} disabled={ listOfOwnedGames[0].includes("dota2")} >Dota2</Button>
                <Button color="primary" onClick={() => onClickRadio(2)} active={radioActive === 2} disabled={ listOfOwnedGames[0].includes("csgo")}>Csgo</Button>
                <Button color="primary" onClick={() => onClickRadio(3)} active={radioActive === 3} disabled={ listOfOwnedGames[0].includes("lol")}>Lol</Button>
            </ButtonGroup>
        </ModalBody>
        <ModalFooter>
            <Button color="secondary" className="float-left" onClick={ toggleModal }>Cancel</Button>
            <Button className="btn btn-success float-right" name="addGame" id="addGame" onClick={ addGameSubmit }>Add Game
                <FontAwesome
                    className='super-crazy-colors ml-2'
                    name='plus'
                    size='1x'
                    style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                />
             </Button>
        </ModalFooter>
        </Form>
        </Modal>
    </div>
  );
};

AddGameForm.PropTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired
};


export default AddGameForm;
