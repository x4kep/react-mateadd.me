import React from 'react';
import '../../styles/dashboard-page.css';

// Since this component is simple and static, there's no parent container for it.
const DashboardPage = () => {
  return (
    <div className="container">
      <div className="pageHeader">
        <h1 className="text-center">Dashboard page</h1>
      </div>
      <h2>Feature: </h2>
      <ol>
        <li>User can add favorite players</li>
        <li>User can block communiction players</li>
        <li>User can post player</li>
        <li>User can post team</li>
        <li>User can post coach</li>
      </ol>
    </div>
  );
};

export default DashboardPage;
