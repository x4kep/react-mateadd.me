import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';

const ProfileExperience = ({experience}) => {
  return (
    <div className="ProfileExperience">
        <div className="ProfileExperience-header">
            <h4>Experience</h4>
        </div>
        <div className="ProfileExperience-body">
            <span>{experience.experience}</span>
        </div>
        {/* <div className="ProfileExperience-footer">
            <Button className="btn btn-success" id={experience.playerId}>+Favorite</Button>
        </div> */}
    </div>
  );
};


export default ProfileExperience;
