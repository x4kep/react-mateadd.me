import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import '../../styles/profile-page.css';
import { Alert } from 'reactstrap';
var FontAwesome = require('react-fontawesome');

//Mini components
import RatingStars from './RatingStars';
import ProfileAvatar from './ProfileAvatar';
import classnames from 'classnames';
import GameSelectorBar from '../common/GameSelectorBar';
import ProfileReview from './ProfileReview';
import ProfileComment from './ProfileComment';
import ProfileGameCard from './ProfileGameCard';
import ProfileExperience from './ProfileExperience';
import ProfileGame from './ProfileGame';
//Bootstrap Componets
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Link } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
//Actions
import * as gameProfileAction from '../../actions/gameProfileAction.js';
import * as commentReviewAction from '../../actions/commentReviewAction.js';
import * as deleteCommentAction from '../../actions/deleteCommentActions.js';

// Since this component is simple and static, there's no parent container for it.
class ProfilePage extends React.Component{
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      gameProfile : Object.assign({}, this.props.gameProfile),
      activeTab: null,
    };
    
    this.onChange = this.onChange.bind(this);
    this.PostReviewComment = this.PostReviewComment.bind(this);
    this.onSubmitDeleteComment = this.onSubmitDeleteComment.bind(this);
  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmitDeleteComment(commentId){
    console.log("onSubmitDeleteComment");
    debugger;
    this.props.actions.deleteCommentAction.DeleteComment(commentId);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  PostReviewComment(senderId, receiverId){
    debugger;
    //TODO GET LOGED IN USER ID
    let text = this.state.profileComment;
    this.props.actions.commentReviewAction.ReviewComment(senderId, receiverId, text);
  }
  componentWillReceiveProps(state){
    debugger;
    //Refresh page Refactor this later
    if(state.commentReview){
      window.location.reload()
    }

    if(state.gameProfile !== []){
      this.setState({
        activeTab: state.gameProfile[0].game.id
      });
    }
      
    
  }
  componentWillMount(){
    // debugger;
    // this.setState({"activeTab":1});
  }
  componentDidMount(){
    debugger;
    let userId = this.props.params.id;
    console.log("componentDidMount ProfilePage");
    console.log("Get userGameProfile" + userId);
    this.props.actions.gameProfileAction.GameProfile(userId);
  }

  render(){
    debugger;
    const {gameProfile} = this.props;
    let gameProfileZero = Object.values(gameProfile);

    // gameProfileZero.map(gameProfile => console.log(gameProfile));
    console.log("GameProfiles");
    console.log(gameProfileZero);
    //Object 
    debugger;
    return (
      <div className=" Profile">
        <div className="pageHeader">
          <h1 className="text-center">View profile</h1>
        </div>
        <div className="Profile-header">
          <div className="row">
            <div className="col-md-3">
              <ProfileAvatar avatar={gameProfileZero}/>
            </div>
            <div className="col-md-7"></div>
            <div className="col-md-2">
              <RatingStars rate={5} />
            </div>
          </div>
        </div>
        <div className="GameSelectorBar">
          {gameProfileZero.map(gameProfile =>
              <div key={ gameProfile.game.id } className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-"+gameProfile.game.shortName, { 'GameSelectorBar-gameIcon-selected': this.state.activeTab == gameProfile.game.id })} onClick={() => { this.toggle(gameProfile.game.id); }}>

              </div>
          )}
        </div>

        <TabContent activeTab={this.state.activeTab}>
          {gameProfileZero.map(gameProfile =>
            <TabPane key={gameProfile.game.id} tabId={gameProfile.game.id}>
              <Row>
                <Col>
                  <div className="row">
                    <div className="col-md-4">
                      <ProfileGameCard gameCard={gameProfile}/>
                      <ProfileExperience experience={gameProfile}/>
                      <ProfileGame game={gameProfile}/>
                    </div>
                    <div className="col-md-8">
                      {sessionStorage.apiToken 
                        ?<ProfileComment logedInUser = {sessionStorage.username} onChange={ this.onChange } onClick={() => { this.PostReviewComment(sessionStorage.userId, gameProfile.id); }} />
                        :<Alert color="info"><strong>Post comment:</strong> You have to be authenticate.</Alert>
                      }
                      {gameProfile.gameProfileComments.map(comment =>
                        <ProfileReview key = {comment.id} user = {gameProfile.player.username}  comment = { comment } onSubmitDeleteComment={ this.onSubmitDeleteComment }/>
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
            </TabPane>
          )}
        </TabContent>
      </div>
    );
  }
};

function mapStateToProps(state, ownProps) {
  console.log("FROM: mapStateToProps");
  console.log(state);

  return {
    profileId: ownProps.params.id,
    gameProfile: state.gameProfile,
    commentReview: state.commentReview
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions:{
      gameProfileAction: bindActionCreators(gameProfileAction, dispatch),
      commentReviewAction: bindActionCreators(commentReviewAction, dispatch),
      deleteCommentAction: bindActionCreators(deleteCommentAction, dispatch)
    }
  };
}


ProfilePage.PropTypes = {
  
};

export default  connect(mapStateToProps, mapDispatchToProps)(ProfilePage);



//TODO
//Get all GameProfilesThat User Owns 
// 
