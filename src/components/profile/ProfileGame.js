import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';

const ProfileGame = ({game}) => {
    debugger;
  return (
    <div className="ProfileGame">
        <div className="ProfileGame-header">
            <h4>Player post</h4>
        </div>
        <div className="ProfileGame-body">
            <div className="ProfileGame-role">
                <h5>Role</h5>
                <div className="ProfileGameRoleItem">                          
                    {game.gameRoles.map(role => 
                        <div className={ game.game.shortName+'-role md '+role.gameRole.value}></div>
                    )}
                </div>
                {/* {game.gameRole.map(role =>  
                    <div className="ProfileGameRoleItem">                          
                        <div className={'dota2-role sm ' + role}></div>
                        <span>{role.name}</span>
                    </div>
                )} */}
            </div>
            <div className="ProfileGame-mode">
                <h5>Mode</h5>
                <div className="ProfileGame-text">
                    {/* <span>{game.gameMode.label}</span> */}
                    {game.gameModes.map(mode => 
                        <span>{mode.gameMode.label}{'  '}</span>         
                    )}
                </div>
            </div>
            <div className="ProfileGame-region">
                <h5>Region</h5>
                <div className="ProfileGame-text">
                    {game.gameRegions.map(mode => 
                        <span>{mode.gameRegion.label}{'  '}</span>         
                    )}
                </div>
            </div>
            <div className="ProfileGame-availability">
                <h5>Availability</h5>
                <div className="ProfileGame-text">
                    <span>{game.availability}</span>
                </div>
            </div>
        </div>
        <div className="ProfileGame-footer">
            
        </div>
    </div>
  );
};


export default ProfileGame;
