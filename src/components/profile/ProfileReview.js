import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';
import Moment from 'react-moment';

import Avatar from '../common/Avatar';
var FontAwesome = require('react-fontawesome');

const ProfileReview = ({user, comment, onSubmitDeleteComment}) => {
  if(comment.player === undefined ){
      return null;
  }
  return (
    <div className="ProfileReview">
        <div className="ProfileReview-header">
            <Avatar  avatarUrl={ 'https://api.adorable.io/avatars/55/' + comment.player.username } name={ comment.player.username }/>
            <span className="pull-right"><Moment format="DD-MMMM-YYYY | HH:MM">{comment.date}</Moment></span>
        </div>
        <div className="ProfileReview-body">
            <span>{ comment.text }</span>
        </div>
        <div className="ProfileReview-footer">
        {
            (sessionStorage.apiToken && comment.player.id == sessionStorage.userId) &&
            <Button className="btn btn-danger float-right" name="deleteGame" id="deleteGame" onClick = { () => onSubmitDeleteComment(comment.id) }>Delete
            <FontAwesome
                className='super-crazy-colors ml-2'
                name='trash '
                size='1g'
                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
            />
            </Button>
        }
        </div>
    </div>
  );
};


export default ProfileReview;
