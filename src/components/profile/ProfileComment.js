import React from 'react';
import { Input, Button } from 'reactstrap';
import { Link } from 'react-router';
import Avatar from '../common/Avatar';

const ProfileComment = ({ logedInUser, onClick, onChange }) => {
  return (
    <div className="ProfileComment">
      <div className="ProfileComment-header ">
        <Avatar  avatarUrl={ 'https://api.adorable.io/avatars/55/' + logedInUser } name={ logedInUser }/>
      </div>
      <div className="ProfileComment-body ">
        <Input type="textarea" rows="4" placeholder="Write your comment" name="profileComment" onChange = {onChange}/>
      </div>
      <div className="ProfileComment-footer ">
        <Button color="success" className="float-right" onClick = {onClick} >Comment</Button>
      </div>
    </div>
  );
};


export default ProfileComment;
