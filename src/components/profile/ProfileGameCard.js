import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';

const ProfileGameCard = ({gameCard}) => {
    debugger;
  return (
    <div className={"ProfileGameCard ProfileGameCard-bg-"+gameCard.game.shortName}>
        <div className="ProfileGameCard-body">
            <div className="ProfileGameCard-avatar">
            <img src={"https://api.adorable.io/avatars/55/"+gameCard.player.username} alt=""/>
            </div>
            <div className="ProfileGameCard-name">
                <h4>{gameCard.player.username}</h4>
            </div>
            <div className="ProfileGameCard-rankImg">
                {/* <img src="http://via.placeholder.com/100x100" alt={gameCard.gameRank.label}/> */}
            </div>
            <div className="ProfileGameCard-rankName">
                <h4>{gameCard.gameRank.label}</h4>
            </div>
            <div className="ProfileGameCard-region">
                <strong>Game Regions: </strong>
                {gameCard.gameRegions.map(region => 
                    <span>{region.gameRegion.label}{' / '}</span>         
                )}
            </div>
        </div>
        <div className="ProfileGameCard-footer">
            <div className="ProfileGameCard-link">
                <a href="#"><strong>Contact me:</strong>{gameCard.contactLink}</a>
            </div>
        </div>
    </div>
  );
};


export default ProfileGameCard;
