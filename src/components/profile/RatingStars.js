import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';

//10 / 2 = 5 zvezda
// 5 / 2 = 2.5 zvezde

const RatingStars = (rate) => {
//   let stars = [];
//   for (let i=0; i<=rate; i++) {
//     stars.push(<i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>);
//   }
  return (
    <div className="RatingStars">
        <div className="RatingStars-row">
            <i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>
            <i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>         
            <i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>
            <i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>
            <i className="fa fa-fw" aria-hidden="true" title="Copy to use star-o"></i>
        </div>
    </div>
  );
};


export default RatingStars;
