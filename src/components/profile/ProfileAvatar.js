import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';
import Moment from 'react-moment';

const ProfileAvatar = ({avatar}) => {
    if (avatar.length !== 0) {
    const date = new Date();
    return (
            <div className="ProfileAvatar">
                <div className="ProfileAvatar-header">
                    <img src={"https://api.adorable.io/avatars/55/"+avatar[0].player.username} alt="" />
                </div>
                <div className="ProfileAvatar-body">
                    <h2>{avatar[0].player.username}</h2>
                </div>
                <div className="ProfileAvatar-footer">
                    <img className={"country-flag country-flag-"+avatar[0].player.country.name} alt=""/>{" "}
                    <span><Moment subtract={{ years: date.getFullYear() }} format="YY">{avatar[0].player.birthday}</Moment></span>
                    <span>{avatar[0].player.country.name}</span>
                </div>
            </div>
        );
    }else{
        return null;
    }
};


export default ProfileAvatar;
