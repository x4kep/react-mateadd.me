import React from 'react';
import '../../styles/home-page.css';
import { Button } from 'reactstrap';
const GameStastistic = (game) => {
  return (
    <div className="GameStastistic" style={{overflow: "auto", "backgroundColor": "rgba(40, 40, 40, 0.8)", color: "white", padding: 10, margin: 10}}>
      <img src="http://via.placeholder.com/50x50" alt={game.gameIcon} style={{float: "left"}}/>
      <div style={{"padding": "0px 10px", float: "left"}}>
        <strong>{game.playerNum}</strong><br/>
        <strong>Players</strong>
      </div>
      <div style={{"padding": "0px 10px", float: "left"}}>
        <strong>{game.teamNum}</strong><br/>
        <strong>Teams</strong>
      </div>
      <div style={{"padding": "0px 10px", float: "left"}}>
        <strong>{game.coachNum}</strong><br/>
        <strong >Coache</strong>
      </div>
    </div> 
  );
};

export default GameStastistic;
