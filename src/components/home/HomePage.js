import React from 'react';
import '../../styles/home-page.css';
import { Button } from 'reactstrap';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

var Carousel = require('react-responsive-carousel').Carousel;
import GameStatistic from './GameStastistic.js';
class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };

  }

  render(){
    console.log(this.state);
    return (
      <div className="HomePage">
          <div className="row">
            <div className="col-sm-12 col-md-6  offset-md-1">
              <h1>The Ultimate Teamfinding Network</h1>
              <ul>
                <li>Join over 350,000 gamers in just minutes</li>
                <li>Find players and teams for the latest and biggest games</li>
                <li>Find a coach who can help you reach the top of the ranks</li>
                <li>Find a team who dream big as you.</li>
                <li>Find a player who have same goal as you.</li>
              </ul>
            </div>
            <div className="col-sm-12 col-md-3">
              <Button color="danger" size="lg" block>JOIN NOW</Button>
            </div>
          </div>
      </div> 
   );
  }
};

function mapStateToProps(state, what){
  debugger;
  return{

  }
}
function mapDispatchToProps(dispatch) {
  return {
    
  };
}


// HomePage.PropTypes = {
//   filterSearch: PropTypes.object.isRequired
// };

export default  connect(mapStateToProps, mapDispatchToProps)(HomePage);
