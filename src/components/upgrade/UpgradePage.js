import React from 'react';
import '../../styles/upgrade-page.css';

// Since this component is simple and static, there's no parent container for it.
const UpgradePage = () => {
  return (
    <div className="container">
      <div className="pageHeader">
        <h1 className="text-center">Upgrade page</h1>
      </div>
      <h2>Feature: </h2>
      <ol>
        <li>User can subscribe</li>
        <li>Bigger Post</li>
        <li>No Adds</li>
      </ol>
    </div>
  );
};

export default UpgradePage;