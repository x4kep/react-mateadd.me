import React from 'react';
import '../../styles/footer.css';
import Summary from './appSummary';
import Menu from './verticalMenu';
import Support from './contactSupport';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render(){
    return (
      <div className="Footer">
        <div className="row">
          <div className="col-md-4">
            <Summary/>
          </div>
          <div className="col-md-4">
            <Menu/>
          </div>
          <div className="col-md-4">
            <Support/>
          </div>
        </div>
      </div>
    );
  };
};

export default Footer;
