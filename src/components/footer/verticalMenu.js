import React from 'react';
import { Link, IndexLink } from 'react-router';

const verticalMenu = (props) => {
  return (
    <div className="verticalMenu">
        <h2>Search player:</h2>
        <ul>
            <li><Link to="/search/dota2" activeClassName="active">Dota2</Link></li>
            <li><Link to="/search/lol" activeClassName="active">Lol</Link></li>
            <li><Link to="/search/csgo" activeClassName="active">CS:GO</Link></li>
        </ul>
    </div>
  );
};


export default verticalMenu;
