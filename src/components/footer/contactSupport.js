import React from 'react';
import { Button } from 'reactstrap';

const contactSupport = (props) => {
  return (
    <div className="contactSupport">
        <h2>Support</h2>
        <Button color="success" size="lg" >Contact support</Button>
        <p>Don't hesitate to ask us questions or give feedback!</p>
    </div>
  );
};


export default contactSupport;
