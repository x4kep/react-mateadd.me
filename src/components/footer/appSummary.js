import React from 'react';

const appSummary = (props) => {
  return (
    <div className="appSummary">
        <div className="appSummary-body">
            <h2>The ultimate teamfinding network</h2>
            <div className="appSummary-logo">
            </div>
            <p>Looking for players or teams for the following games?</p>
            <p>CS:GO, League of Legends, Dota 2</p>
            <p>We’ll connect you to gamers from around the world!</p>
        </div>
        <div className="appSummary-footer">
            <h5>Copyright © 2017 DVDEV</h5>
        </div>
    </div>
  );
};


export default appSummary;
