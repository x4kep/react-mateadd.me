import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, FormText  } from 'reactstrap';

const LoginForm = ({onClick, isOpen, onChange, onSubmit, errors, errorsServer}) => {
  // debugger;
  return (
    <div className="LoginForm" >
      <Button color="primary" onClick={onClick}>Login</Button>                 
      <Modal isOpen={isOpen}>
        <form onSubmit={onSubmit}>
        <ModalHeader>Login</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Email / Username</Label>
            <Input onChange={onChange} name="loginName" type="text" placeholder=""  />
            <FormText color="danger">
              {errors.userName}
            </FormText>
          </FormGroup>

          <FormGroup>
            <Label>Password</Label>
            <Input onChange={onChange} name="loginPassword" type="password" placeholder=""  />   
            <FormText color="danger">
              {errors.userPassword}
              {errorsServer}
            </FormText>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onClick}>Cancel</Button>
          <Button color="primary" type="submit">Login</Button>
        </ModalFooter>
        </form>
      </Modal>
    </div>
  );
};

LoginForm.PropTypes = {
  onClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired
};


export default LoginForm;
