import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, FormText  } from 'reactstrap';

const RegisterForm = ({onClick, isOpen, onChange, onSubmit, errors, errorsServer}) => {
  return (
    <div className="RegisterForm" >
      <Button color="secondary" onClick={onClick}>Register</Button>
      <Modal isOpen={isOpen}>
        <form onSubmit={onSubmit}>
        <ModalHeader>Register</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Email</Label>
            <Input onChange={onChange} name="registerEmail" type="text" placeholder=""  />   
            <FormText color="danger">
              {errors.userEmail}
            </FormText>
          </FormGroup>
          <FormGroup>
            <Label>Display username</Label>
            <Input onChange={onChange} name="registerUsername" type="text" placeholder=""  />   
            <FormText color="danger">
              {errors.userName}
              {errorsServer}
            </FormText>
          </FormGroup>
          <FormGroup>
            <Label>Password</Label>
            <Input onChange={onChange}  name="registerPassword" type="password" placeholder=""  /> 
            <FormText color="danger">
              {errors.userPassword}
            </FormText>
          </FormGroup>
          <FormGroup>
            <Label>RePassword</Label>
            <Input onChange={onChange} name="registerRePassword" type="password" placeholder=""  />
            <FormText color="danger">
              {errors.userRePassword}
            </FormText>
          </FormGroup> 
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onClick }>Cancel</Button>
          <Button color="primary" type="submit">Register</Button>
        </ModalFooter>
        </form>
      </Modal>
    </div>
  );
};

RegisterForm.PropTypes = {
  onClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired
};


export default RegisterForm;
