import React from 'react';
import {classnames} from 'classnames';

const GameSelectorBar = (props) => {
  return (
     <div className="GameSelectorBar">
        <div className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-dota2", { 'GameSelectorBar-gameIcon-selected': this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }}>
        
        </div>
        <div className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-lol", { 'GameSelectorBar-gameIcon-selected': this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }}>
        
        </div>
        <div className={classnames("GameSelectorBar-gameIcon","GameSelectorBar-gameIcon-csgo", { 'GameSelectorBar-gameIcon-selected': this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }}>
        
        </div>
    </div>
  );
};


export default GameSelectorBar;
