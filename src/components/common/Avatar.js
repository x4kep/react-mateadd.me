import React from 'react';

const Avatar = (props) => {
  return (
    <div className="Avatar">
      <img src={props.avatarUrl} alt={props.name}/>
      <span>{props.name}</span>
    </div>
  );
};


export default Avatar;
