import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../../actions/authActions';
import { Link, IndexLink } from 'react-router';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, FormText  } from 'reactstrap';
// import axios from 'axios';
import '../../styles/header.css';
import Avatar from './Avatar.js';
import LoginForm from './LoginForm.js';
import RegisterForm from './RegisterForm.js';
import Loader from './Loader.js';
// import MenuRole from './MenuRole.js';

import toastr from 'toastr';
import toastrOptions from '../../options/toastOptions.js';
toastr.options = toastrOptions;
//TODO
//appUser: for api info

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalLogin: false,
      modalRegister: false,
      loginName: "",
      loginPassword: "",
      registerEmail: "",
      registerUsername: "",
      registerPassword: "",
      registerRePassword: "",
      errors: {
        userName: "",
        userPassword: "",
      }
    };

    this.toggleModalLogin = this.toggleModalLogin.bind(this);
    this.toggleModalRegister = this.toggleModalRegister.bind(this);
    this.onChange = this.onChange.bind(this);

    this.onSubmitLogin = this.onSubmitLogin.bind(this);
    this.onSubmitRegister = this.onSubmitRegister.bind(this);
    this.onSubmitLogout = this.onSubmitLogout.bind(this);

  }


  toggleModalLogin() {
    console.log(this);
    this.setState({
      modalLogin: !this.state.modalLogin,
      errors: {},
      errorServer: ""
    });
  }

  toggleModalRegister() {
    console.log(this);
    this.setState({
      modalRegister: !this.state.modalRegister,
      errors: {},
      errorServer: ""
    });
  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmitLogin(e){
    e.preventDefault();
    //TODO Validation
    //debugger;
    if(!this.loginFormIsValid()){
      return;
    }
    // this.setState({ modalLogin: false });
    this.props.actions.login(this.state);
  }


  loginFormIsValid(){
    let formIsValid = true;
    let errors = {};
    let userName = this.state.loginName;
    let userPassword = this.state.loginPassword;

    if(userName.length === 0){
      errors.userName = "Username can't be empty.";
      formIsValid = false;
    }else if(userName.length < 3){
      formIsValid = false;
      errors.userName = "Username length can't be less then 3 letters.";
    }

    if(userPassword.length === 0){
      errors.userPassword = "Password can't be empty.";
      formIsValid = false;
    }else if(userPassword.length < 3){
      formIsValid = false;
      errors.userPassword = "Password length can't be less then 3 letters.";
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  onSubmitRegister(e){
    e.preventDefault();
    //TODO Validation
    if(!this.registerFormIsValid()){
      return;
    }
    // this.setState({ modalRegister: false });
    this.props.actions.register(this.state);
  }


  registerFormIsValid(){
    let formIsValid = true;
    let errors = {};
    let userEmail = this.state.registerEmail;
    let userName = this.state.registerUsername;
    let userPassword = this.state.registerPassword;
    let userRePassword = this.state.registerRePassword;
    
    //Email validation
    if(userEmail.length === 0){
      errors.userEmail = "Email can't be empty.";
      formIsValid = false;
    }else if(userEmail.length < 3){
      formIsValid = false;
      errors.userEmail = "Email length can't be less then 3 letters.";
    }else if(validateEmail(userEmail)){
      formIsValid = false;
      errors.userEmail = "Email is not valid.";
    }
    function validateEmail(email) {
      var reExp = /[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/;
      return !reExp.test(email);
    }

    //Username validation
    if(userName.length === 0){
      errors.userName = "Username can't be empty.";
      formIsValid = false;
    }else if(userName.length < 3){
      formIsValid = false;
      errors.userName = "Username length can't be less then 3 letters.";
    }else if(validateUsername(userName)){
      formIsValid = false;
      errors.userName = "Your username is not valid. Only characters A-Z, a-z, length 3-15 and numbers are  acceptable.";
    }
    function validateUsername(userName){
      var reExp =  /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,15}$/;
      return !reExp.test(userName);
    }

    if(userPassword.length === 0){
      errors.userPassword = "Password can't be empty.";
      formIsValid = false;
    }else if(userPassword.length < 3){
      formIsValid = false;
      errors.userPassword = "Password length can't be less then 3 letters.";
    }else if(validatePassword(userPassword)){
      formIsValid = false;
      errors.userPassword = "Your password is not valid. At least one uppercase letter and one number, length min 8 letters";
    }

    function validatePassword(userPassword){
      var reExp =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
      return !reExp.test(userPassword);
    }

    //Reg exp for password

    if(userRePassword.length === 0){
      errors.userRePassword = "Password can't be empty.";
      formIsValid = false;
    }else if(userRePassword.length < 3){
      formIsValid = false;
      errors.userRePassword = "Password length can't be less then 3 letters.";
    }else if(validatePassword(userRePassword)){
      formIsValid = false;
      errors.userRePassword = "Your password is not valid. Only letters, numbers and underscores are allowed, length 6-15.";
    }else if(userPassword !== userRePassword){
      formIsValid = false;
      errors.userRePassword = "Password doesn't match.";
    }
    //Reg exp for password
    //Is this two password same

    this.setState({errors: errors});
    return formIsValid;
  }
  
  onSubmitLogout(e){
    e.preventDefault();
    this.props.actions.logout();
  }

  componentWillReceiveProps(nextProps){
    console.log("componentWillReceiveProps");


    //REFACTOR: This is bad practice to Set State From Props 
    //Set server side error
    if(nextProps.userData.errorMessage !== ""){
      this.setState({
        errorServer: nextProps.userData.errorMessage
      });
    }
    //Close modal
    if(!nextProps.userData.modalState){
      this.setState({
        "modalLogin":false
      });
    }
  }
  

  componentDidMount(){
    console.log("componentDidMount");
    console.log("Here login user agan, get data from session");
    //On Refresh Login user from session
   

    //Prevent losing state on refresh
    if(!!sessionStorage.username || !!sessionStorage.password){
      let userData={};
      userData.loginName = sessionStorage.username;
      userData.loginPassword = sessionStorage.password;
      this.props.actions.login(userData);
    }
  }
  

  render(){
    const {userData} = this.props;
    return (
      <nav className="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse navbar-main">
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <a className="navbar-brand" href="#"><img className="Logo" src="../../media/MateaddmeLogo.png" alt="Mateadd.me logo" style={{width: 50, height: 50}}/></a>
        <div className="collapse navbar-collapse" id="navbarCollapse">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <IndexLink  className="nav-link" to="/" activeClassName="active">Home</IndexLink>
            </li>
            <li className="nav-item">
              <Link  className="nav-link" to="/search" activeClassName="active">Search player</Link>
            </li>
            <li className="nav-item">
              <Link  className="nav-link"to="/about" activeClassName="active">About</Link>
            </li>
          </ul>
          <ul className="navbar-nav float-right">
               <li className="nav-item dropdown">
                {!this.props.userData.userLogedIn &&
                  <div>
                    <LoginForm 
                        onClick={this.toggleModalLogin}
                        isOpen={this.state.modalLogin}
                        onChange={this.onChange}
                        onSubmit={this.onSubmitLogin}
                        errors={this.state.errors}
                        errorsServer = {this.state.errorServer}
                      />

                      <RegisterForm
                        onClick={this.toggleModalRegister}
                        isOpen={this.state.modalRegister}
                        onChange={this.onChange}
                        onSubmit={this.onSubmitRegister}
                        errors={this.state.errors}
                        errorsServer = {this.state.errorServer}
                      />
                  </div>                
                }
               </li> 
              {this.props.userData.isFetching && 
                <Loader/>
              }
              
              {this.props.userData.userLogedIn &&             
                this.props.userData.userRole === "admin" &&
                <span style={{display:"inherit"}}>
                    <li className="nav-item dropdown">
                        <Link className="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <Avatar avatarUrl={"https://api.adorable.io/avatars/55/"+this.props.userData.userName} name={this.props.userData.userName} ></Avatar>
                        </Link>
                        <div className="dropdown-menu" aria-labelledby="dropdown01">
                            <Link className="dropdown-item" to="/admin/user" activeClassName="active">Users</Link>
                            {/* <Link className="dropdown-item" to="/admin/comment" activeClassName="active">Comments</Link>
                            <Link className="dropdown-item" to="/admin/support" activeClassName="active">Support</Link> */}
                            <Link className="dropdown-item" to="/#" onClick={this.onSubmitLogout}>Logout</Link>
                        </div>
                    </li>
                </span>
              }

              {this.props.userData.userLogedIn &&             
                this.props.userData.userRole === "user" &&
                <span style={{display:"inherit"}}>
                  <li className="nav-item dropdown">
                      <Link className="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <Avatar avatarUrl={"https://api.adorable.io/avatars/55/"+this.props.userData.userName} name={this.props.userData.userName} ></Avatar>
                      </Link>
                      <div className="dropdown-menu" aria-labelledby="dropdown01">
                          <Link className="dropdown-item" to="/setting" activeClassName="active">Setting</Link>
                          {/* <Link className="dropdown-item" to="/dashboard" activeClassName="active">Dashboard</Link>
                          <Link className="dropdown-item" to="/upgrade" activeClassName="active">Upgrade</Link> */}
                          <Link className="dropdown-item" to="/#" onClick={this.onSubmitLogout}>Logout</Link>
                      </div>
                  </li>
                </span>
              }

          </ul>      
        </div>
        <div>
        </div>
      </nav>
    );
  };
};

function mapStateToProps(state, ownProps) {
  // debugger;
  return {
    userData: state.authReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  };
}

//Pull in the React Router context so router is available on this.context.router.
Header.contextTypes = {
  router: PropTypes.object
};

Header.PropTypes = {
  actions: PropTypes.object.isRequired,
  router: React.PropTypes.func.isRequired
};

export default  connect(mapStateToProps, mapDispatchToProps)(Header);
