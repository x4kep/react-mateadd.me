import React from 'react';
import '../../styles/about-page.css';

// Since this component is simple and static, there's no parent container for it.
const AboutPage = () => {
  return (
    <div>
      <div className="container">
        <div className="pageHeader">
          <h1 className="text-center">About page</h1>
        </div>
        <div className="row">
          <div className="col-md-12">
            <p>When it comes to Dota 2, a lot of players describe the community with very negative words like unfriendly, flaming, raging and other kinds. You will have to agree that this is the reality. The reason for this is that in no other game you
              will suffer as much as in Dota 2 when one of your teammates is bad (or just unexperienced). So you might agree that it is those "noobs" to blame.</p>
          <p>But it is not as simple as that. Everyone starts out as a "noob" and will gradually improve, but to what degree we improve depends on our motivation and the environment we are playing in. A lot of players play the game to "own". By that
              I mean you enjoy Dota 2 when you win - and you rage or hate when you lose. Dota 2 by its nature favours that kind of behaviour, of that I am convinced by experience. Additionally it's easier for anyone to blame mistakes on others since
              it's more convenient. This creates a very dangerous foundation.</p>
          <p>Whenever I ask new players I brought to Dota 2, they all say they like the game, but they dislike the people that play it.</p>
          <p>So this kind of environment makes it a lot harder for people to actually improve. Everyone blames others for their mistakes instead of looking at oneself. And this is where the "blamer" limits himself in becoming a better player. You just
              don't improve if you only flame and blame and think you're better than you actually are (The Dunning-Kruger Effect ). I admit it. I was like that too. I played DotA since the early Warcraft 3: Reign of Chaos version referred to as
              "Classic DotA" and I was really good. At least I thought. Blaming others and flaming when they made mistakes and enjoying stomping Pubs with a stacked Party, yeah, that was fun. That is Dota, own or get owned.</p>
            </div>
          </div>
      </div>
    </div>
  );
};

export default AboutPage;
