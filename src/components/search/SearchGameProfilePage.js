import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types'
import * as filterSearchAction from '../../actions/filterSearchAction';
import * as filterGameProfileAction from '../../actions/filterGameProfileAction';

import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import PostCard from './PostCard';
import SearchForm from './SearchForm';
import '../../styles/search-page.css';
import LazyLoad  from 'react-lazyload';
import { StickyContainer, Sticky } from 'react-sticky';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

// Since this component is simple and static, there's no parent container for it.
class SearchGameProfilePage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      filterSearch : Object.assign({}, props.filterSearch),
      filterGameProfileResult : Object.assign({}, props.filterGameProfileResult),
      searchFormState:{
        currentGame: "",
        rank: "",
        mode: "",
        language: "",
        country: "",
        region: "",
        minage: "",
        maxage: "",
        role: [],
        gender: null
      },
    };

    
    this.updateSearchFormState = this.updateSearchFormState.bind(this);
    this.onSubmitClear = this.onSubmitClear.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  

  updateSearchFormState(event){
    debugger;
    let value = event.target.value;
    let formState = this.state.searchFormState;
    let currentGame = this.props.routeParams.game;
    let initStateSearchForm = {
                                currentGame: formState.currentGame,
                                rank: formState.rank,
                                language: formState.language,
                                country: formState.country, 
                                mode: formState.mode,
                                region: formState.region,
                                minage: formState.minage,
                                maxage: formState.maxage,
                                role: formState.role
                              };
                              
    const field = event.target.name; 
    let searchFormState = Object.assign({}, initStateSearchForm);

    if(event.target.type === "checkbox"){
      if(event.target.checked === true){
        searchFormState[field].push(value);
      }else{
        let itemIndex = searchFormState[field].indexOf(value);
        searchFormState[field].splice(itemIndex, 1);
      }
    }else{
      searchFormState[field] = value;
    }
    return this.setState({searchFormState: searchFormState});
  }

  onSubmitClear(){
    console.log('onSubmitClear');
    let initStateSearchForm = {
        currentGame: "",
        rank: "",
        language: "",
        country: "", 
        region: "",
        minage: "",
        maxage: "",
        role: []
    };
    let searchFormState = Object.assign({}, initStateSearchForm);
    return this.setState({searchFormState: searchFormState});
  }

  onSubmitFilter(){
    console.log('onSubmitFilter');
    let currentGame = this.props.routeParams.game;
    debugger;
    // let params = {
    //   gameId:1,
    //   gameModeId:[15,14,12],
    //   gameRegionId:[31],
    //   gameRoleId:[15],
    //   languageId:[1,2],
    //   countryId:2,
    //   gamerankId:15,
    //   gender:"male",
    //   age:[0,40]
    // }

    //Create params for API
    var gameId = 1;
    if(currentGame === "csgo"){
      gameId = 2;
    }else if(currentGame === "lol"){
      gameId = 3;
    }

    let params = {
      gameId:       gameId || undefined,
      gameRankId:   ((this.state.searchFormState.rank === "")        ? undefined : parseInt(this.state.searchFormState.rank)),
      gameModeId:   ((this.state.searchFormState.mode === "")        ? undefined : [parseInt(this.state.searchFormState.mode)]),
      gameRegionId: ((this.state.searchFormState.region === "")      ? undefined : [parseInt(this.state.searchFormState.region)]),
      gameRoleId:   ((this.state.searchFormState.role.length === 0)  ? undefined : [this.state.searchFormState.role]),
      languageId:   ((this.state.searchFormState.language === "")    ? undefined : [parseInt(this.state.searchFormState.language)]),
      countryId:    parseInt(this.state.searchFormState.country)     || undefined,
      gender:       this.state.searchFormState.gender || undefined,
      age:          ((this.state.searchFormState.minage === "" || this.state.searchFormState.maxage === "" )  ? undefined : [parseInt(this.state.searchFormState.minage), parseInt(this.state.searchFormState.maxage)]),
    }
    



    params =JSON.stringify([params]);

    //[{"gameId":1},{"gameModeId":[15,14,12]},{"gameRegionId":[31]},{"gameRoleId":[15]},{"languageId":[1,2]},{"countryId":2},{"gameRankId":15},{"gender":"male"},{"age":[0,40]}]
    this.props.actions.filterGameProfileAction.filterGameProfile(params, currentGame);
  }
  
  componentDidMount(){
    console.log("componentDidMount");
    let currentGame = this.props.routeParams.game;
    this.props.actions.filterGameProfileAction.filterGameProfileInit(currentGame);
  }

  render(){
    let gameIndex = 0;
    let currentGame = this.props.routeParams.game;
    let gameProfiles =this.props.filterGameProfileResult;

    //Stop rendering if there aren't loaded games
    if( this.props.filterSearch === undefined || gameProfiles === undefined ){
      return (<div>Loading...</div>);
    }

    gameProfiles = Object.values(gameProfiles);

    console.log(gameProfiles);
    debugger;

    //Set gameIndex
    
    if(currentGame === "csgo"){
      gameIndex = 1;  
    }else if(currentGame === "lol"){
      gameIndex = 2;
    }

    return (
          <div>
            <h1>Search { currentGame } players</h1>
            <div className="row">
              <div className="col-md-3">
                {/* SEARH FORM Component */}
                <SearchForm gameform={ this.props.filterSearch }
                            gameIndex={ gameIndex }
                            onSubmitClear = { this.onSubmitClear } 
                            onSubmitFilter = { this.onSubmitFilter } 
                            onChange =  {this.updateSearchFormState }/>
              </div>
              {/* SEARH Component */}
              <div className="col-md-9">
                <div className="row">
                    { gameProfiles.map(profile =>
                      <div className="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12">
                      <LazyLoad height={250} offset={0}>
                        <PostCard 
                          gameName = { currentGame }                    
                          id={ profile.player.id }
                          country={ profile && profile.player && profile.player.country && profile.player.country.code }
                          gameRank={ profile && profile.gameRank && profile.gameRank.value}
                          userImg={ profile.player.username }
                          userName={  profile.player.username }
                          gameMode={ [ profile.gameModes ] }
                          gameRole={ [ profile.gameRoles ] }
                        />
                      </LazyLoad >
                      </div>
                    ) }
                </div>
              </div>
            </div>
          </div>
        );
  }
};


function mapStateToProps(state, ownProps) {
  console.log("FROM: mapStateToProps");
  console.log(state);
  debugger;
  return {
    filterSearch: state.filterSearchAction[0],
    filterGameProfileResult: state.filterGameProfile[0]
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      getSearchForm: bindActionCreators(filterSearchAction, dispatch),
      filterGameProfileAction: bindActionCreators(filterGameProfileAction, dispatch),
    }
  };
}


SearchGameProfilePage.PropTypes = {
  filterSearch: PropTypes.array.isRequired,
  filterGameProfileResult: PropTypes.array.isRequired,
};

export default  connect(mapStateToProps, mapDispatchToProps)(SearchGameProfilePage);