import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
var FontAwesome = require('react-fontawesome');

const SearchForm = ({gameform, gameIndex, onSubmitClear, onSubmitFilter, onChange}) => {
  debugger;
  return (
    <div className="SearchForm">
        <Form>
            <div className="row">
                <div className="col-md-12">
                    <FormGroup>
                        <Label for="rank">Rank</Label>
                        <Input type="select" name="rank" id="rank" onChange={onChange}>
                        <option value="">Select</option>
                        {gameform.game[gameIndex].gameRanks.map(rank => 
                            <option key={rank.id} value={rank.id}>{rank.label}</option>    
                        )}
                        </Input>
                    </FormGroup>    
                </div>
            </div>
            <FormGroup>
                <Label for="mode">What are you looking to play?</Label>
                <Input type="select" name="mode" id="mode" onChange={onChange}>
                <option value="">Select</option>
                {gameform.game[gameIndex].gameModes.map(mode => 
                    <option key={mode.id} value={mode.id}>{mode.label}</option>    
                )}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="language">Language</Label>
                <Input type="select" name="language" id="language" onChange={onChange}>
                <option value="">Select</option>
                {gameform.language.map(language => 
                    <option  key={language.id} value={language.id}>{language.label}</option>    
                )}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="country">Country</Label>
                <Input type="select" name="country" id="country" onChange={onChange}>
                <option value="">Select</option>
                {gameform.country.map(country => 
                    <option key={country.id} value={country.id}>{country.label}</option>  
                )}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="region">Region</Label>
                <Input type="select" name="region" id="region" onChange={onChange}>
                <option value="">Select</option>
                {gameform.game[gameIndex].gameRegions.map(region => 
                    <option key={region.id} value={region.id}>{region.label}</option>    
                )}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="gender">Gender</Label>
                <Input type="select" name="gender" id="gender">
                    <option value="">Select</option>
                    <option value={"male"}>Male</option>
                    <option value={"female"}>Female</option>
                </Input>
            </FormGroup>
            <div className="row">
                <div className="col-md-6">
                    <FormGroup>
                        <Label for="minage">Min Age</Label>
                        <Input type="select" name="minage" id="minage" onChange={onChange}>
                        <option value="">Select</option>
                        {gameform.age.map(age => 
                            <option key={age.id} value={age}>{age}</option>    
                        )}
                        </Input>
                    </FormGroup>
                </div>
                <div className="col-md-6">
                    <FormGroup>
                        <Label for="maxage">Max Age</Label>
                        <Input type="select" name="maxage" id="maxage" onChange={onChange}>
                        <option value="">Select</option>   
                        {gameform.age.map(age => 
                            <option key={age.id} value={age}>{age}</option>    
                        )}
                        </Input>
                    </FormGroup>   
                </div>
            </div>
            <FormGroup tag="fieldset">
                <legend>Game Role</legend>
                <div className="row">
                    {gameform.game[gameIndex].gameRoles.map(role => 
                        <div className="col-md-6 col-sm-12">
                            <FormGroup check>
                            <Label check>
                                <Input type="checkbox" name="role" key={role.id} value={role.id} onChange={onChange}/>{' '}{role.label}
                            </Label>
                            </FormGroup>
                        </div>   
                    )}
                </div>
            </FormGroup>
            {/* <Button color="danger" onClick={onSubmitClear}>Clear</Button> */}
            <Button color="success" className="float-right" onClick={onSubmitFilter} style={{"width":"100%", "height":"50px"}}>Filter
                <FontAwesome
                    className='ml-3'
                    name='search'
                    size='1x'
                    style={{ textShadow: '1px 3px 0px rgba(0, 0, 0, 0.4)' }}
                />
            </Button>
        </Form>    
    </div>
  );
};


export default SearchForm;
