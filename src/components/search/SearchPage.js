import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as filterSearchAction from '../../actions/filterSearchAction';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import '../../styles/searchGame-page.css';
import GameStatsCard from './GameStatsCard';

// Since this component is simple and static, there's no parent container for it.
class SearchPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  
  
  render(){
     return (
            <div>
              <div className="pageHeader">
                <h1 className="text-center">Which game are you here for?</h1>
                <h5 className="text-center">Click one of the cards below.</h5>
              </div>
              <div className="GameStatsCard-container">
                <div className="row">
                  <div className="col-sm col-xs-12 col-lg-2 offset-lg-3">
                    <GameStatsCard tag="dota2" title="Dota 2" countPlayer="415"/>
                  </div>
                  <div className="col-sm col-xs-12 col-lg-2">
                    <GameStatsCard tag="lol" title="League of Legends" countPlayer="515"/>
                  </div>
                  <div className="col-sm col-xs-12 col-lg-2">
                    <GameStatsCard tag="csgo" title="Counter Strike" countPlayer="1415"/>
                  </div>
                </div>
              </div>
            </div>
          );
  }
};

function mapStateToProps(state, ownProps) {
  return {
    filterSearch: state.filterSearchAction
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(filterSearchAction, dispatch)
  };
}


SearchPage.PropTypes = {
  filterSearch: PropTypes.object.isRequired
};

export default  connect(mapStateToProps, mapDispatchToProps)(SearchPage);

// http://localhost:100/explorer/#!/GameProfile/GameProfile_count {"gameId":1}