import React from 'react';
import { Button } from 'reactstrap';
import { Link, IndexLink } from 'react-router';

const GameStatsCard = ({tag, title, countPlayer}) => {
  return (  
        <Link to={ '/search/' + tag }>
            <div className={ 'GameStatsCard GameStatsCard-bg-' + tag }>
                    <div className={ 'GameStatsCard-header GameStatsCard-logo-' + tag }>
                        
                    </div>
                    <div className="GameStatsCard-body">
                        <h3>{ title }</h3>
                    </div>
                    <div className="GameStatsCard-footer">
                        <p>{ countPlayer + ' '} players</p>
                    </div>
            </div>
        </Link>
  );
};


export default GameStatsCard;

