import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router';
var FontAwesome = require('react-fontawesome');

const PostCard = (post) => {
    debugger;
  return (
    <div className="PostCard">
        <div className="PostCard-content">
            <div className="PostCard-header">
                <div className="PostCard-header-country">
                    <img className={"country-flag country-flag-"+post.country} alt=""/>
                </div>
                <div className="PostCard-header-rank">
                    {/* <img src="http://via.placeholder.com/40x40" alt={post.gameRank}/> */}
                    <span>{post.gameRank}</span>
                </div>
            </div>
            <div className="PostCard-body">
                <div className="PostCard-body-profileImg">
                    <img src={"https://api.adorable.io/avatars/55/abot@"+post.userImg+".png"} alt=""/>
                </div>
                <div className="PostCard-body-profileName">
                    <h5>{post.userName}</h5>
                </div>
                <div className="PostCard-body-profileInterest">
                        {post.gameMode[0].map(mode =>
                            <span key={mode.gameMode.id}>{mode.gameMode.value +" "}</span>
                        )}
                </div>
                <div className="PostCard-body-gameRole">
                    {post.gameRole[0].map(role =>
                        <div key={role.gameRole.id} className={ post.gameName+'-role sm ' + role.gameRole.value }></div>
                    )}
                </div>
            </div>
            <div className="PostCard-footer">
                {/* <Button color="" className="btn btn-sm">Message</Button> */}
                <Link className="" to={"/profile/" + post.id}>
                    <Button color="primary" className="btn btn-primary" style={{width: "100%"}}>
                        Profile
                        <FontAwesome
                            className='ml-3'
                            name='user-o'
                            size='1x'
                            style={{ textShadow: '1px 3px 0px rgba(0, 0, 0, 0.1)' }}
                            inverse
                        />
                    </Button>
                </Link>
            </div>
        </div>
    </div>
  );
};


export default PostCard;
