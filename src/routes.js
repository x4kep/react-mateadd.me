import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import SearchPage from './components/search/SearchPage';
import SearchGameProfilePage from './components/search/SearchGameProfilePage';
import ProfilePage from './components/profile/ProfilePage';
import DashboardPage from './components/dashboard/DashboardPage';
import UpgradePage from './components/upgrade/UpgradePage';
import SettingPage from './components/setting/SettingPage';
import UserPage from './components/admin/user/UserPage';
import CommentPage from './components/admin/comment/CommentPage';
import SupportPage from './components/admin/support/SupportPage';

import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="about" component={AboutPage}/>
    <Route path="search" component={SearchPage}/>
    <Route path="search/:game" component={SearchGameProfilePage}/>
    <Route path="setting" authRole="user" onEnter={requireAuth} component={SettingPage}/>
    <Route path="dashboard" onEnter={requireAuth} component={DashboardPage}/>
    <Route path="upgrade" onEnter={requireAuth} component={UpgradePage}/>
    <Route path="profile" onEnter={requireAuth} component={ProfilePage}/>
    <Route path="profile/:id" component={ProfilePage}/>
    <Route path="/admin/user" authRole="admin" onEnter={requireAuth} component={UserPage}/>
    <Route path="/admin/comment" authRole="admin" onEnter={requireAuth} component={CommentPage}/>
    <Route path="/admin/support" authRole="admin" onEnter={requireAuth} component={SupportPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

function requireAuth(nextState, replace) {  
  let role = nextState.routes[1].authRole;
  let logedIn = !!sessionStorage.apiToken;
  let roleSession = sessionStorage.userRole;

  if(role === "admin"){
    if(!(roleSession === role && logedIn)){
      debugger;
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      })
    }
  }

  if(role === "user"){
    debugger;
    if(!(roleSession === role && logedIn)){
      debugger;
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      })
    }
  }


}


