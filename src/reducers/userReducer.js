import { USER_SUCCESS, BANUSER_SUCCESS } from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function userReducer(state = initialState.users, action) {
  switch (action.type) {
    case USER_SUCCESS:
    return Object.assign({}, action.response.data)
    case BANUSER_SUCCESS:
    return Object.assign({}, action.response.data)
    default:
      return state;
  }
}



