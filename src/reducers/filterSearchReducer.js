import {GET_GAME_SUCCESS} from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function filterSearchReducer(state = initialState.filterSearch, action) {
  switch (action.type) {
    case GET_GAME_SUCCESS:
    return [...state, Object.assign({}, action.response)]
    //  return Object.assign({}, state, action.response);
    default:
      return state;
  }
}



