export default {
  userData:{
    userName: "",
    userImage: "",
    userApiKey: "",
    userId: "",
    userEmail: "",
    userLogedIn:false,
    errorMessage: "",
    isFetching: false,
    reducerName: "",
    userRole:"",
    modalState: false
  },
  filterSearch:[],
  gameProfile:[],
  basicProfile:[],
  users:[],
  comments:[],
  commentReview: false
};