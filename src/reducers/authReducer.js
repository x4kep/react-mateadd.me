import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_ERROR, REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_ERROR, LOGOUT_SUCCESS, LOGOUT_REQUEST , LOGOUT_ERROR} from '../constants/actionTypes';
// import objectAssign from 'object-assign';
import initialState from './initialState';

export default function authReducer(state = initialState.userData, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
    return Object.assign({}, state,
       {
        userLogedIn: action.isAuthenticated,
        isFetching: action.isFetching,
        modalState: true
       }
      );
    case LOGIN_SUCCESS:
    debugger;
    return Object.assign({}, state, 
      {
        userLogedIn: action.isAuthenticated,
        userId: action.user.data.id,
        userName: action.user.data.username,
        userImage: action.user.data.username,
        userEmail: action.user.data.email,
        userRole: action.user.data.role,
        userApiKey: sessionStorage.apiToken,
        isFetching: action.isFetching,
        modalState: false,
        errorMessage: "",
        reducerName: "LOGIN_SUCCESS",
      }
    );
    case LOGIN_ERROR: 
    debugger;
    return Object.assign({}, state, 
      {
        userLogedIn: action.isAuthenticated,
        errorMessage: "Wrong password or username",
        isFetching: action.isFetching,
        modalState: true
      }
    );
    
    case REGISTER_REQUEST:
    return Object.assign({}, state, {
      isFetching: action.isFetching,
    });

    case REGISTER_SUCCESS: 
    debugger;  
    return Object.assign({}, state, {
      isFetching: action.isFetching,
      userLogedIn: false,
      userId: "",
      userName: "",
      userImage: "",
      userEmail: "",
      modalState: false,
      reducerName: "REGISTER_SUCCESS"
    });

    case REGISTER_ERROR: 
    debugger;
    return Object.assign({}, state, {
      isFetching: action.isFetching,
      errorMessage: action.message
    });

    case LOGOUT_REQUEST:
    return Object.assign({}, state, {
      isFetching: action.isFetching,
    });

    case LOGOUT_SUCCESS:
    return Object.assign({}, state, {
        userLogedIn: false,
        userId: "",
        userName: "",
        userImage: "",
        userEmail: "",
        modalState: false
      }
    );

    case LOGOUT_ERROR: 
    debugger;
    return Object.assign({}, state, {
      isFetching: action.isFetching,
    });

    default:
      return state;
  }
}

