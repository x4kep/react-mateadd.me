import { BASICPROFILE_SUCCESS } from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function gameProfileReducer(state = initialState.basicProfile, action) {
  switch (action.type) {
    case BASICPROFILE_SUCCESS:
    return Object.assign({}, state, action.response.data)
    default:
      return state;
  }
}


