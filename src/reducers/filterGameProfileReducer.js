import { FILTERGAMEPROFILEINIT_SUCCESS, FILTERGAMEPROFILE_SUCCESS } from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function filterGameProfileInit(state = [], action) {
    switch (action.type) {
        case FILTERGAMEPROFILEINIT_SUCCESS:
        return [Object.assign({}, action.response.data)]
        case FILTERGAMEPROFILE_SUCCESS:
        return [Object.assign({}, action.response.data)]
        default:
            return state;
    }
}



