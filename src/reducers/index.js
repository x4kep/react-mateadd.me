import { combineReducers } from 'redux';
import {routerReducer} from 'react-router-redux';
import authReducer from './authReducer';
import filterSearchAction from './filterSearchReducer';
import gameProfile from './gameProfileReducer';
import basicProfile from './basicProfileReducer';
import users from './userReducer';
import comments from './commentReducer';
import commentReview from './commentReviewReducer';
import deleteGame from './deleteGameReducer';
import addGame from './settingReducers/addGameReducer';
import updateBasicProfile from './settingReducers/updateBasicProfileReducer';
import filterGameProfile from './filterGameProfileReducer';

const rootReducer = combineReducers({
  authReducer,
  filterSearchAction,
  gameProfile,
  basicProfile,
  users,
  comments,
  commentReview,
  deleteGame,
  addGame,
  updateBasicProfile,
  filterGameProfile,
  routing: routerReducer,
});

export default rootReducer;
