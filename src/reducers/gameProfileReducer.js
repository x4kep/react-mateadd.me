import {GAMEPROFILE_SUCCESS, REVIEWCOMMENT_SUCCESS } from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function gameProfileReducer(state = initialState.gameProfile, action) {
  switch (action.type) {
    case GAMEPROFILE_SUCCESS:
    return Object.assign({}, action.response.data)
    default:
      return state;
  }
}


