//Game


/*
    Language
    Country
    AgeMin
    AgeMax
    Gender
*/


//DOTA
var Game = [
  {
   platform: ['Steam'],
   region: ['India', 'US / West', 'US / East','Europe / West', 'Europe / East'],
   rank: [{'solo':2000, 'party': 3000}],
   role: ['Carry', 'Mid', 'Offlane', 'Farmin support', 'Hard support'],
   playstyle: ['Aggresive/Fighter', 'Pusher', 'Initiator', 'Ganker', 'Roamer', 'Jungler', 'Passive/Farmer'],
   playerInterested: ['Unranked/Normal', 'Ranked', 'Leagues/Turnaments']
  },
];

//Lol
var Game = [
  {
   platform: ['Game'],
   region: ['North America', 'EU west', 'EU Nordic & East', 'Korea'],
   rank: ['Bronze', 'Silver',  'Gold', 'Platinum'],
   role: ['Top', 'Mid', 'Support', 'Jungle', 'ADC', 'Fill'],
   playstyle: ['Aggresive/Fighter', 'Pusher', 'Initiator', 'Ganker', 'Roamer', 'Jungler', 'Passive/Farmer'],
   playerInterested: ['Unranked3v3', 'Unranked5v5', 'Ranked3v3', 'Ranked5v5', 'ARAM']
  },
];

//Overwatch
var Game = [
  {
   platform: ['Battle net', 'Xbox Live', 'PSN'],
   region: ['United State', 'Europe', '', ''],
   rank: ['Bronze', 'Silver',  'Gold', 'Platinum'],
   role: ['DPS', 'Flex', 'Tank', 'Support'],
   playstyle: [],
   playerInterested: ['Quick Play', 'Commpetitive Play', 'Leagues/Turnaments']
  },
];